Le patrimoine de Golfe du Morbihan Vannes Agglomération (GMVA) a
évolué ces dernières années à la fois en quantité mais aussi en
technicité. Le suivi et l’amélioration des performances énergétiques des
installations communautaires doit permettre de réduire les
consommations énergétiques et ainsi atteindre les objectifs collectifs
du Plan Climat Air Energie territorial, notamment de diminuer de 30%
les consommations du territoire à horizon 2030 par rapport à 2012.

L’objectif du projet proposé est d’analyser les données de suivi des
installations électriques, chauffage, ventilation, climatisation en
vue de l’amélioration de l’efficacité énergétique du patrimoine et le
suivi des installations de production d’énergie renouvelable de la
collectivité et ainsi proposer les modules de régulation et des
systèmes d’alerte pouvant être fonction, par exemple de données de
prévisions météorologiques, etc.

Dans ce projet, il s'agit de fournir des prévisions météorologiques et
de les présenter graphiquement. Ces données pourront être stockées
dans une base de données pour conserver un historique. Cet historique
permettra de vérifier si les prévisions étaient correctes et de
vérifier si les ajustements opérés sur les installations électriques,
chauffage, ventilation, climatisation ont été pertinentes en fonction
des prévisions et des relevés réels.  Différentes sources
météorologiques pourront être considérées
(e.g. https://meteofrance.com/, https://www.wunderground.com/,
https://weather.com/, https://www.met.no/, https://www.windy.com/,
...)
